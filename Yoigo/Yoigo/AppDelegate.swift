//
//  AppDelegate.swift
//  Yoigo
//
//  Created by Daniel Moraleda on 21/2/17.
//  Copyright © 2017 ThinkSmart. All rights reserved.
//

import UIKit
import App4OneSDK


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var appCore: AppCore?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window?.makeKeyAndVisible()
        
        self.appCore = AppCore.sharedInstance
        
        guard let safeAppCore = self.appCore else { return true }
        
        safeAppCore.appConfig = AppConfigModel(name: "App4One", version: "1.0", initialScreen: "Login", screens: [
            
            "TabBar"   : Screen(name: "TabBar", resource: "CustomTabBarController", navigable: true, type: .TabBar, order: [
                Order(resourceName: "Home",     iconName: "Icono Inicio", name: "Inicio"),
                Order(resourceName: "Catalog",  iconName: "Icono Catálogo", name: "Catálogo"),
                Order(resourceName: "Points",   iconName: "Icono Puntos", name: "Puntos"),
                //                                Order(resourceName: "Messages", iconName: "Icono Actividad", name: "Mensajes"),
                Order(resourceName: "Empty",    iconName: "Icono Extras", name: "Extras"),
                ], events: nil, package: "App4OneSDK"),
            
            "SideMenu" : Screen(name: "SideMenu", resource: "JDSideMenu", navigable: false, type: .SideMenu, order: [
                Order(resourceName: "Empty", iconName: "", name: ""),
                Order(resourceName: "TabBar", iconName: "", name: ""),
                Order(resourceName: "Panel", iconName: "", name: "")
                ], events: nil, package: "App4OneSDK"),
            
            "Prueba"   : Screen(name: "Prueba",  resource: "PruebaViewController", navigable: true, type: .ViewController, order: nil, events: [
                "touchInButton"  : Event(name: "touchInButton", destination: "Prueba2", type: .Push),
                "returnToSide"   : Event(name: "returnToSide", destination: "SideMenu", type: .Dismiss)
                ], package: "App4OneSDK"),
            
            "Prueba2"  : Screen(name: "Prueba2", resource: "Prueba2ViewController", navigable: false, type: .ViewController, order: nil, events: [
                "touhcInButtonPop" : Event(name: "touhcInButtonPop", destination: "", type: .Pop)], package: "App4OneSDK"),
            
            "Panel"  : Screen(name: "Panel", resource: "PanelViewController", navigable: false, type: .ViewController, order: nil, events: [
                "touchUpInsideSalir"        : Event(name: "touchUpInsideSalir", destination: "Login", type: .ChangeRoot),
                "touchUpInsideEditProfile"  : Event(name: "touchUpInsideEditProfile", destination: "EditProfile", type: .Present),
                "goToNotifications"  : Event(name: "goToNotifications", destination: "Notifications", type: .Present)
                ], package: "App4OneSDK"),
            
            "Login"    : Screen(name: "Login", resource: "LoginViewController", navigable: true, type: .ViewController, order: nil, events: [
                "touhcInButtonLogin" : Event(name: "touhcInButtonLogin", destination: "SelectProfile", type: .Push),
                "goToChangeKey"      : Event(name: "goToChangeKey", destination: "ForgotPassword", type: .Push),
                "goToChangePass"      : Event(name: "goToChangePass", destination: "ChangePassword", type: .Push)
                ], package: "App4OneSDK"),
            
            "Home"     : Screen(name: "Home", resource: "HomeViewController" , navigable: true, type: .ViewController, order: nil, events: [
                "showProductDetail"     : Event(name: "showProductDetail", destination: "ProductDetail", type: .Push),
                "showInitialInsertData" : Event(name: "showInitialInsertData", destination: "InitialInsertData", type: .Present),
                "fromHomeToIndicators"  : Event(name: "fromHomeToIndicators", destination: "CarStoreDataNav", type: .Present), /*fromHomeToSelectors*/
                "fromHomeToSelectors"  : Event(name: "fromHomeToSelectors", destination: "ZoneSelector", type: .Present)
                ], package: "App4OneSDK"),
            "SelectProfile" : Screen(name: "SelectProfile", resource: "SelectProfileViewController", navigable: false, type: .ViewController, order: nil, events: [
                "continueToHome" : Event(name: "continueToHome", destination: "SideMenu", type: .ChangeRoot)
                ], package: "App4OneSDK"),
            "Catalog"  : Screen(name: "Catalog", resource: "CatalogViewController", navigable: true, type: .ViewController, order: nil, events: [
                "showProductDetail" : Event(name: "showProductDetail", destination: "ProductDetail",    type: .Push),
                "gotToShoppingCart" : Event(name: "gotToShoppingCart", destination: "ShoppingCart",     type: .Push),
                "goToCategories"    : Event(name: "goToCategories",    destination: "Categories",       type: .FormSheet)
                ], package: "App4OneSDK"),
            "ProductDetail" : Screen(name: "ProductDetail", resource: "ProductDetailViewController", navigable: false, type: .ViewController, order: nil, events: [
                "backToCatalog": Event(name: "backToCatalog", destination: "Catalog", type: .Pop),
                "gotToShoppingCart" : Event(name: "gotToShoppingCart", destination: "ShoppingCart", type: .Push)
                ], package: "App4OneSDK"),
            "EditProfile" : Screen(name: "EditProfile", resource: "EditProfileViewController", navigable: true, type: .ViewController, order: nil, events: [
                "returnToSide"         : Event(name: "returnToSide", destination: "SideMenu", type: .Dismiss),
                "goToChangePassword"   : Event(name: "goToChangePassword", destination: "ChangePassword", type: .Push)
                ], package: "App4OneSDK"),
            "ForgotPassword" : Screen(name: "ForgotPassword", resource: "ForgotPasswordViewController", navigable: false, type: .ViewController, order: nil, events: [
                /*"goToSelectProfile" : Event(name: "goToSelectProfile", destination: "SelectProfile", type: .Push),*/
                "goLogin" : Event(name: "goLogin", destination: "Login", type: .Pop)
                ], package: "App4OneSDK"),
            "ChangePassword" : Screen(name: "ChangePassword", resource: "ChangePasswordViewController", navigable: false, type: .ViewController, order: nil, events: [
                "goToSelectProfile" : Event(name: "goToSelectProfile", destination: "SelectProfile", type: .Push),
                "logoutToLogin" : Event(name: "logoutToLogin", destination: "Login", type: .Pop)
                ], package: "App4OneSDK"),
            "InitialInsertData" : Screen(name: "InitialInsertData", resource: "InitialInsertDataViewController", navigable: false, type: .ViewController, order: nil, events: [
                "returnToHome"   : Event(name: "returnToHome", destination: "SideMenu", type: .Dismiss)
                ], package: "App4OneSDK"),
            "Messages" : Screen(name: "Messages", resource: "MessagesViewController", navigable: true, type: .ViewController, order: nil, events:[
                "gotToNewMessage" : Event(name: "gotToNewMessage", destination: "NewMessage", type: .FormSheet)
                ], package: "App4OneSDK"),
            "NewMessage" : Screen(name: "NewMessage", resource: "NewMessageViewController", navigable: false, type: .ViewController, order: nil, events: [
                "backToMessages": Event(name: "backToMessages", destination: "Messages", type: .DismissFormSheet),
                ], package: "App4OneSDK"),
            "ShoppingCart" : Screen(name: "ShoppingCart", resource: "ShoppingCartViewController", navigable: false, type: .ViewController, order: nil, events: [
                "backToCatalog"      : Event(name: "backToCatalog", destination: "Catalog", type: .Pop),
                "goToShipmentAdress" : Event(name: "goToShipmentAdress", destination: "ShipmentAdress", type: .Push),
                "showProductDetail"     : Event(name: "showProductDetail", destination: "ProductDetail", type: .Push)
                ], package: "App4OneSDK"),
            "ShipmentAdress" : Screen(name: "ShipmentAdress", resource: "ShipmentAdressViewController", navigable: false, type: .ViewController, order: nil, events: [
                "backToShoppingCart" : Event(name: "backToShoppingCart", destination: "ShoppingCart", type: .Pop),
                "goToConfirmShoppingCart" : Event(name: "goToConfirmShoppingCart", destination: "ConfirmShoppingCart", type: .Push)
                ], package: "App4OneSDK"),
            "ConfirmShoppingCart" : Screen(name: "ConfirmShoppingCart", resource: "ConfirmShoppingCartViewController", navigable: false, type: .ViewController, order: nil, events: [
                "backToShipmentAdress" : Event(name: "backToShipmentAdress", destination: "ShipmentAdress", type: .Pop),
                "goToShoppingFinished" : Event(name: "goToShoppingFinished", destination: "ShoppingFinished", type: .Push)
                ], package: "App4OneSDK"),
            "ShoppingFinished"    : Screen(name: "ShoppingFinished", resource: "ShoppingFinishedViewController", navigable: false, type: .ViewController, order: nil, events: [
                "goToCatalog" : Event(name: "goToCatalog", destination: "Catalog", type: .PopToRoot)
                ], package: "App4OneSDK"),
            "Points" : Screen(name: "Points", resource: "PointsViewController", navigable: true, type: .ViewController, order: nil, events: [
                "goToDetail" : Event(name: "goToDetail", destination: "PointsDetail", type: .FormSheet)
                ], package: "App4OneSDK"),
            "PointsDetail" : Screen(name: "PointsDetail", resource: "PointsDetailViewController", navigable: false, type: .ViewController, order: nil, events: [
                "backToPoints": Event(name: "backToPoints", destination: "Points", type: .DismissFormSheet),
                ], package: "App4OneSDK"),
            "Notifications" : Screen(name: "Notifications", resource: "NotificationsViewController", navigable: true, type: .ViewController, order: nil, events: [
                "returnToSide"   : Event(name: "returnToSide", destination: "SideMenu", type: .Dismiss)
                ], package: "App4OneSDK"),
            "Categories" : Screen(name: "Categories", resource: "CategoryViewController", navigable: false, type: .ViewController, order: nil, events: [
                "goToSubCategories"   : Event(name: "goToSubCategories", destination: "SubCategories", type: .Push),
                "backToCatalog" : Event(name: "backToCatalog", destination: "Catalog", type: .DismissFormSheet)
                
                ], package: "App4OneSDK"),
            "SubCategories" : Screen(name: "SubCategories", resource: "SubCategoryViewController", navigable: false, type: .ViewController, order: nil, events: [
                "goToCatalogAndFilter"   : Event(name: "goToCatalogAndFilter", destination: "Catalog", type: .DismissFormSheetToRoot),
                "backToCategories" : Event(name: "backToCategories", destination: "Categories", type: .Pop)
                ], package: "App4OneSDK")
            ])
        
        safeAppCore.customModel = CustomModel(
            
            primaryColor: UIColor(rgba:"#3F51B5"),
            secundaryColor: UIColor(rgba: "#FF9800"),
            auxColor: UIColor(rgba:"#303F9F"),
            overPrimaryColor: UIColor(rgba: "#FFFFFF"),
            overSecundaryColor: UIColor(rgba: "#FFFFFF"),
            overAuxColor: UIColor(rgba: "#ffffff"),
            
            navBarColor: UIColor(rgba:"#3F51B5"),
            
            
            tabBarColor: UIColor(rgba: "#FFFFFF"),
            tabBarSelectedColor: UIColor(rgba: "#212121"),
            tabBarUnselectedColor : UIColor(rgba: "#757575"),
            
            loginStatusBarLight: false,
            
            // Add Flag and StringArray with the localizations enabled to each version
            defaultLocalization: true,
            localizationsEnabled: [""],
            
            shippingDataModify: true,
            
            halCashEnabled: false,
            touchIdEnabled: true,
            orderTrackingEnabled: false,
            
            buttonsStyles: [
                
                .Positive : ButtonStyle(text: "Positive",   textColor: .whiteColor(), imageURL: nil, color: UIColor(rgba: "#048743"), cornerRadius: 1),
                .Negative : ButtonStyle(text: "Negative",   textColor: .whiteColor(), imageURL: nil, color: UIColor(rgba: "#048743"), cornerRadius: 1),
                .Default  : ButtonStyle(text: "Default",    textColor: .whiteColor(), imageURL: nil, color: UIColor(rgba: "#048743"), cornerRadius: 1)
            ]
        )
        
        safeAppCore.startEngine()
        
        //        appCore.sessionManager.setCurrentShipment(AOShoppingCart(id: 1, products: [
        //            AOProduct(id: 1, name: "botella de agua", brand: "Font Vella")
        //            ]))
        //
        //        appCore.sessionManager.getCurrentShipment()?.addProduct(AOProduct(id: 2, name: "botella de agua", brand: "bezoya"))
        //        appCore.sessionManager.getCurrentShipment()?.removeProduct(appCore.sessionManager.getCurrentShipment()!.products![0])
        //
        //        print("\(appCore.sessionManager.getCurrentShipment())")
        
        return true
    }
    
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        print("** AppDelegate didRegisterForRemoteNotificationsWithDeviceToken")
        self.appCore?.didReceiveToken(deviceToken)
    }
    
    func application(application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        //self.appCore.notificationHandler.failedToRegisterForNotifications(error)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject],
                     fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        //self.appCore?.application(application, didReceiveRemoteNotification: userInfo, fetchCompletionHandler: completionHandler)
    }
}



